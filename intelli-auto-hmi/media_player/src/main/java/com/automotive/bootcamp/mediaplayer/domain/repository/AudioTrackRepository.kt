package com.automotive.bootcamp.mediaplayer.domain.repository

import com.automotive.bootcamp.mediaplayer.domain.model.AudioTrack
import kotlinx.coroutines.flow.Flow

interface AudioTrackRepository {

    val favouriteAudioTracks: Flow<List<AudioTrack>>

    val onlineAudioTracks: Flow<List<AudioTrack>>

    val localAudioTracks: Flow<List<AudioTrack>>

    val recentTracks: Flow<List<AudioTrack>>

    val playlists: Flow<List<String>>

    suspend fun retrieveOnlineAudioTrack()

    suspend fun retrieveLocalAudioTrack()

    suspend fun toggleTrackFavouriteStatus(track: AudioTrack)

    suspend fun appendRecentTrack(track: AudioTrack)

    suspend fun removeRecentTrack(track: AudioTrack)

    suspend fun searchAudioTrack(word: String): List<AudioTrack>

    suspend fun createNewPlaylist(playlistName: String)

    suspend fun retrieveTrackByPlaylist(playlistName: String): List<AudioTrack>

    suspend fun addNewTrackToPlaylist(playlistId: Int, trackId: Int)

    suspend fun deletePlaylist(playlistName: String)

    // Media stuff

    suspend fun retrieveOnlineAudioTracks(): List<AudioTrack>

    suspend fun retrieveLocalAudioTracks(): List<AudioTrack>

    suspend fun retrieveFavoriteTracks(): List<AudioTrack>

    suspend fun retrieveTrackById(mediaId: Int): AudioTrack?

    suspend fun retrieveRecentTracks(): List<AudioTrack>
}
