package com.automotive.bootcamp.mediaplayer.domain

enum class AudioLocation {
    ONLINE, LOCAL
}
