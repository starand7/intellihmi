package com.automotive.bootcamp.mediaplayer.data.database.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

// List if all online and local tracks
@Entity(tableName = "audio_metadata")
data class AudioMetadataDB(
    @ColumnInfo(name = "id") val id: String,
    @ColumnInfo(name = "title") val title: String,
    @ColumnInfo(name = "album") val album: String,
    @ColumnInfo(name = "artist") val artist: String,
    @ColumnInfo(name = "genre") val genre: String,
    @ColumnInfo(name = "source") val source: String,
    @ColumnInfo(name = "image") val image: String,
    @ColumnInfo(name = "duration") val duration: Int,
    @ColumnInfo(name = "location") val location: Int,
    @ColumnInfo(name = "favourite") var favourite: Boolean = false
) {
    @PrimaryKey(autoGenerate = true) @ColumnInfo(name = "track_id") var trackId: Int = 0
}

// List of recent track
@Entity(tableName = "recent_tracks")
data class RecentTrackDB(
    @PrimaryKey @ColumnInfo(name = "recent_track_id") val recentTrackId: Int
)

// List of playlists
@Entity(tableName = "track_playlist")
data class PlaylistDB(
    @ColumnInfo(name = "playlist_name") val playlistName: String
) {
    @PrimaryKey(autoGenerate = true) @ColumnInfo(name = "playlist_id") var playlistId: Int = 0
}

// List CrossRef playlists and tracks
@Entity(primaryKeys = ["playlist_id", "track_id"])
data class PlaylistTracksDB(
    @ColumnInfo(name = "playlist_id") val playlistId: Int,
    @ColumnInfo(name = "track_id") val trackId: Int
)
