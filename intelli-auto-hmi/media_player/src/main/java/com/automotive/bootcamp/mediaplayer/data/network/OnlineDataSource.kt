package com.automotive.bootcamp.mediaplayer.data.network

import com.automotive.bootcamp.mediaplayer.domain.model.AudioTrack


interface OnlineDataSource {

    suspend fun fetchAudioTracks(): List<AudioTrack>
}
