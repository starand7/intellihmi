package com.automotive.bootcamp.mediaplayer.presentation.other

import androidx.fragment.app.Fragment
import com.automotive.bootcamp.mediaplayer.R
import com.automotive.bootcamp.mediaplayer.domain.model.AudioTrack
import com.automotive.bootcamp.mediaplayer.exoplayer.library.Origin
import com.automotive.bootcamp.mediaplayer.presentation.player.PlayerFragment

fun Fragment.startTrackFragment(track: AudioTrack, origin: Origin) {
    startTrackFragment(track.fileId, origin)
}

fun Fragment.startTrackFragment(mediaId: Int, origin: Origin) {
    parentFragment?.parentFragmentManager
        ?.beginTransaction()
        ?.replace(R.id.navigation_container, PlayerFragment.newInstance(mediaId, origin))
        ?.addToBackStack(null)
        ?.commit()
}

//fun Fragment.startSettingsFragment() {
//    childFragmentManager
//        .beginTransaction()
//        .replace(R.id.media_player_container, SettingsFragment())
//        .addToBackStack(null)
//        .commit()
//}
