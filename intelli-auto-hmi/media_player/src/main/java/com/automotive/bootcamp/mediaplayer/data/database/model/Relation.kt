package com.automotive.bootcamp.mediaplayer.data.database.model

import androidx.room.Embedded
import androidx.room.Junction
import androidx.room.Relation

data class PlaylistWithTracks(
    @Embedded val playlist: PlaylistDB,
    @Relation(
        parentColumn = "playlist_id",
        entityColumn = "track_id",
        associateBy = Junction(PlaylistTracksDB::class)
    )
    val tracks: List<AudioMetadataDB>
)
