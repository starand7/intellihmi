package com.automotive.bootcamp.mediaplayer.data.local.mapper

import com.automotive.bootcamp.mediaplayer.data.local.model.LocalAudioMetadata
import com.automotive.bootcamp.mediaplayer.domain.model.AudioTrack
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

fun LocalAudioMetadata.toAudioTrack(): AudioTrack {
    return AudioTrack(
        id = id.toString(),
        title = title,
        artist =  artist,
        album = album,
        image = image.toString(),
        genre = genre,
        duration = duration.toInt(),
        source = source
    )
}
