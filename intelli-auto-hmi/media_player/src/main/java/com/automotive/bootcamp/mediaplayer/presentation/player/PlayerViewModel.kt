package com.automotive.bootcamp.mediaplayer.presentation.player

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.automotive.bootcamp.mediaplayer.domain.model.AudioTrack
import com.automotive.bootcamp.mediaplayer.domain.repository.AudioTrackRepository
import com.automotive.bootcamp.mediaplayer.exoplayer.MusicServiceConnection
import com.automotive.bootcamp.mediaplayer.exoplayer.library.Origin
import com.automotive.bootcamp.mediaplayer.presentation.tabControl.TabControlViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.launch

class PlayerViewModel(
    private val mediaId: Int,
    private val origin: String,
    private val repository: AudioTrackRepository
) : ViewModel() {

    private val _tracksStateFlow = MutableStateFlow<List<AudioTrack>>(emptyList())
    val tracksStateFlow = _tracksStateFlow.filter { it.isNotEmpty() }

    init {
        viewModelScope.launch {
            val currentTrack = repository.retrieveTrackById(mediaId) ?: error("AudioTrack cannot be null here (mediaId = $mediaId)")
            val tracks = when (origin) {
                Origin.LOCAL.name -> repository.retrieveLocalAudioTracks()
                Origin.ONLINE.name -> repository.retrieveOnlineAudioTracks()
                Origin.RECENT.name -> repository.retrieveRecentTracks()
                Origin.FAVORITES.name -> repository.retrieveFavoriteTracks()
                else -> { error("Out of Origin") }
            }.toMutableList()
            tracks.removeIf { it.fileId == currentTrack.fileId }
            tracks.add(0, currentTrack)
            _tracksStateFlow.emit(tracks)
        }
    }
}
