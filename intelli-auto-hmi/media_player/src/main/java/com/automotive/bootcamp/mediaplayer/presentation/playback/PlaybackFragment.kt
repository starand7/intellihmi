package com.automotive.bootcamp.mediaplayer.presentation.playback

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.Bundle
import android.os.IBinder
import android.util.Log
import android.view.View
import androidx.core.view.isInvisible
import androidx.fragment.app.Fragment
import com.automotive.bootcamp.mediaplayer.R
import com.automotive.bootcamp.mediaplayer.databinding.FragmentPlaybackBinding
import com.automotive.bootcamp.mediaplayer.domain.model.AudioTrack
import com.automotive.bootcamp.mediaplayer.exoplayer.utils.PlayerService
import com.automotive.bootcamp.mediaplayer.util.delegate.viewBindings
import com.bumptech.glide.Glide

class PlaybackFragment : Fragment(R.layout.fragment_playback) {

    private val binding by viewBindings(FragmentPlaybackBinding::bind)

    private var playerService: PlayerService? = null
    private val connection = object : ServiceConnection {
        override fun onServiceConnected(name: ComponentName?, binder: IBinder?) {
            playerService = (binder as PlayerService.PlayerBinder).service
            playerService?.currentTrack?.observe(
                this@PlaybackFragment.viewLifecycleOwner,
                this@PlaybackFragment::fillPlayerView
            )
            playerService?.isPlaying?.observe(
                this@PlaybackFragment.viewLifecycleOwner,
                this@PlaybackFragment::fillPlaybackView
            )
        }

        override fun onServiceDisconnected(name: ComponentName?) {
            Log.d(TAG, "onServiceDisconnected: $name")
            playerService = null
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupViews()
        connectToService()
    }

    private fun setupViews() {
        setupTogglePlaybackButton()
        setupNextButton()
        setupPreviousButton()
        setupShuffleButton()
    }

    private fun setupTogglePlaybackButton() {
        binding.ibTogglePlayback.setOnClickListener {
            playerService?.togglePlayback()
        }
    }

    private fun setupNextButton() {
        binding.ibNext.setOnClickListener {
            playerService?.next()
        }
    }

    private fun setupPreviousButton() {
        binding.ibPrevious.setOnClickListener {
            playerService?.previous()
        }
    }

    private fun setupShuffleButton() {
        binding.ibShuffle.setOnClickListener {
            playerService?.shuffle()
        }
    }

    private fun connectToService() {
        val serviceIntent = Intent(requireContext(), PlayerService::class.java)
        requireActivity().apply {
            bindService(serviceIntent, connection, Context.BIND_AUTO_CREATE)
            startService(serviceIntent)
        }
    }

    private fun fillPlaybackView(isPlaying: Boolean) {
        val imageRes = if (isPlaying) R.drawable.ic_pause else R.drawable.ic_play
        binding.ibTogglePlayback.setImageResource(imageRes)
    }

    private fun fillPlayerView(track: AudioTrack) {
        with(binding) {
            container.isInvisible = false
            Glide.with(ivAlbumArt)
                .load(track.image)
                .into(ivAlbumArt)
            tvTitle.text = track.title
            tvArtist.text = track.artist
        }
    }

    companion object {

        private val TAG = PlaybackFragment::class.java.simpleName

        fun newInstance() = PlaybackFragment()
    }
}
