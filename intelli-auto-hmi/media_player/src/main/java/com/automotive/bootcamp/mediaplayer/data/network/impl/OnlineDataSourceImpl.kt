package com.automotive.bootcamp.mediaplayer.data.network.impl

import com.automotive.bootcamp.mediaplayer.data.network.OnlineDataSource
import com.automotive.bootcamp.mediaplayer.data.network.api.UampApi
import com.automotive.bootcamp.mediaplayer.data.network.mapper.toAudioTrack
import com.automotive.bootcamp.mediaplayer.domain.model.AudioTrack

class OnlineDataSourceImpl(
    private val api: UampApi
) : OnlineDataSource {

    override suspend fun fetchAudioTracks(): List<AudioTrack> {
        return api.fetchAudioTracks().toAudioTrack()
    }
}
