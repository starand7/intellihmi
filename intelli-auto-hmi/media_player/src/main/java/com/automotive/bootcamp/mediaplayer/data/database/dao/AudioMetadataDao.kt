package com.automotive.bootcamp.mediaplayer.data.database.dao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Transaction
import com.automotive.bootcamp.mediaplayer.data.database.model.AudioMetadataDB
import com.automotive.bootcamp.mediaplayer.data.database.model.PlaylistDB
import com.automotive.bootcamp.mediaplayer.data.database.model.PlaylistTracksDB
import com.automotive.bootcamp.mediaplayer.data.database.model.PlaylistWithTracks
import com.automotive.bootcamp.mediaplayer.data.database.model.RecentTrackDB
import kotlinx.coroutines.flow.Flow

@Dao
interface AudioMetadataDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun addAudioMetadata(newAudioMetadata: List<AudioMetadataDB>)

    @Query("SELECT * FROM audio_metadata WHERE location=:location")
    fun getAudioMetadata(location: Int): Flow<List<AudioMetadataDB>>

    @Query("SELECT * FROM audio_metadata WHERE track_id IN (:ids)")
    suspend fun getTrackListByIds(ids: List<Int>): List<AudioMetadataDB>

    @Query(
        "SELECT * FROM audio_metadata WHERE title LIKE :query OR album LIKE :query OR artist LIKE :query"
    )
    suspend fun searchAudioTrack(query: String): List<AudioMetadataDB>

    @Query("UPDATE audio_metadata SET favourite = :favourite WHERE track_id = :track_id")
    suspend fun updateFavouriteStatus(track_id: Int, favourite: Boolean)

    @Query("SELECT * FROM audio_metadata WHERE favourite = 1")
    fun getFavouriteTracks(): Flow<List<AudioMetadataDB>>

    // Media Service

    @Query("SELECT * FROM audio_metadata WHERE location=:location")
    suspend fun getAudioTracksByLocation(location: Int): List<AudioMetadataDB>

    @Query("SELECT * FROM audio_metadata WHERE favourite = 1")
    suspend fun getFavoriteTracks(): List<AudioMetadataDB>
}

@Dao
interface RecentTracksDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun addRecentTrack(track: RecentTrackDB)

    @Query("SELECT * FROM recent_tracks")
    fun getRecentTracks(): Flow<List<RecentTrackDB>>

    @Delete
    suspend fun removeRecentTrack(track: RecentTrackDB)

    @Query("SELECT * FROM recent_tracks")
    suspend fun getRecentTrack(): List<RecentTrackDB>
}

@Dao
interface AudioPlaylistDao {
    @Insert(onConflict = OnConflictStrategy.ABORT) // make alert
    suspend fun addNewPlaylist(playlistName: PlaylistDB)

    @Query("SELECT * FROM track_playlist")
    fun getAudioPlaylist(): Flow<List<PlaylistDB>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun addPlaylistItem(crossRef: PlaylistTracksDB)

    @Transaction
    @Query("SELECT * FROM track_playlist WHERE playlist_name=:playlistName")
    suspend fun getPlaylistTracks(playlistName: String): List<PlaylistWithTracks>

    @Delete
    suspend fun removeAudioPlaylist(playlist: PlaylistDB)

    @Transaction
    @Query("DELETE FROM track_playlist WHERE playlist_name=:playlistName")
    suspend fun deletePlaylistTracks(playlistName: String)
}
