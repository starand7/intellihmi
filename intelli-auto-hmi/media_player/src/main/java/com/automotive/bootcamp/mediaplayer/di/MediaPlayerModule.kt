package com.automotive.bootcamp.mediaplayer.di

import android.content.ComponentName
import com.automotive.bootcamp.mediaplayer.data.database.AppDatabase
import com.automotive.bootcamp.mediaplayer.data.local.LocalDataSource
import com.automotive.bootcamp.mediaplayer.data.local.impl.LocalDataSourceImpl
import com.automotive.bootcamp.mediaplayer.data.local.metadata.LocalAudioTrackDataSource
import com.automotive.bootcamp.mediaplayer.data.network.OnlineDataSource
import com.automotive.bootcamp.mediaplayer.data.network.api.UampApi
import com.automotive.bootcamp.mediaplayer.data.network.impl.OnlineDataSourceImpl
import com.automotive.bootcamp.mediaplayer.data.repository.AudioTrackRepositoryImpl
import com.automotive.bootcamp.mediaplayer.domain.repository.AudioTrackRepository
import com.automotive.bootcamp.mediaplayer.exoplayer.MediaPlaybackService
import com.automotive.bootcamp.mediaplayer.exoplayer.MusicServiceConnection
import com.automotive.bootcamp.mediaplayer.exoplayer.utils.TrackPlayer
import com.automotive.bootcamp.mediaplayer.presentation.media.MediaViewModel
import com.automotive.bootcamp.mediaplayer.presentation.player.PlayerViewModel
import com.automotive.bootcamp.mediaplayer.presentation.playlist.PlaylistViewModel
import com.automotive.bootcamp.mediaplayer.presentation.tabControl.TabControlViewModel
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.qualifier.named
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

val networkModule = module {

    single(named("BaseUrl")) {
        // TODO: move to gradle with new api url
        "https://storage.googleapis.com/"
    }

    single {
        Moshi.Builder()
            .add(KotlinJsonAdapterFactory())
            .build()
    }

    single {
        OkHttpClient.Builder()
            .addInterceptor(
                HttpLoggingInterceptor().apply {
                    HttpLoggingInterceptor.Level.BODY
                }
            )
            .build()
    }

    single {
        Retrofit.Builder()
            .client(get())
            .addConverterFactory(MoshiConverterFactory.create())
            .baseUrl(get<String>(named("BaseUrl")))
            .build()
            .create(UampApi::class.java)
    }

    single<OnlineDataSource> { OnlineDataSourceImpl(api = get()) }
}

val databaseModule = module {

    single { AppDatabase.getInstance(androidContext()) }
}

val repositoryModule = module {

    single { LocalAudioTrackDataSource(context = androidContext()) }

    single<LocalDataSource> { LocalDataSourceImpl(localAudioTrack = get()) }

    single<AudioTrackRepository> {
        AudioTrackRepositoryImpl(
            database = get(),
            onlineDataSource = get(),
            localDataSource = get()
        )
    }
}

val viewModelModule = module {
    viewModel { params ->
        MediaViewModel(
            mediaId = params.get(),
            audioTrackRepository = get()
        )
    }
    viewModel { params ->
        PlayerViewModel(
            mediaId = params.get(),
            origin = get(),
            repository = get()
        )
    }
    viewModel { TabControlViewModel(get()) }
    viewModel { PlaylistViewModel() }
}

val playerModule = module {

    single {
        MusicServiceConnection(
            context = androidContext(),
            ComponentName(androidContext(), MediaPlaybackService::class.java)
        )
    }

    single { TrackPlayer(androidContext()) }
}

val mediaPlayerModule = networkModule + databaseModule + repositoryModule + viewModelModule + playerModule
