package com.automotive.bootcamp.mediaplayer.domain.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class AudioTrack(
    val id: String,
    val title: String,
    val album: String,
    val artist: String,
    val genre: String,
    val source: String?,
    val image: String,
    val duration: Int,
    var favourite: Boolean = false,
    var fileId: Int = 0
) : Parcelable
