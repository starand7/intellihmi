package com.automotive.bootcamp.mediaplayer.presentation.playlist

import androidx.fragment.app.Fragment
import com.automotive.bootcamp.mediaplayer.R

class PlaylistFragment : Fragment(R.layout.fragment_playlists)
