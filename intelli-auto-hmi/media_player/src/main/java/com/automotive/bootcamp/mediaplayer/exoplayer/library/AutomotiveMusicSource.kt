package com.automotive.bootcamp.mediaplayer.exoplayer.library

import android.media.browse.MediaBrowser
import android.support.v4.media.MediaDescriptionCompat
import android.support.v4.media.MediaMetadataCompat
import com.automotive.bootcamp.mediaplayer.domain.model.AudioTrack
import com.automotive.bootcamp.mediaplayer.domain.repository.AudioTrackRepository
import com.automotive.bootcamp.mediaplayer.exoplayer.*
import com.automotive.bootcamp.mediaplayer.presentation.other.KEY_FAVORITE
import com.automotive.bootcamp.mediaplayer.presentation.other.KEY_ORIGIN
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.awaitAll
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.withContext
import java.util.concurrent.TimeUnit

class AutomotiveMusicSource(
    private val repository: AudioTrackRepository
) : AbstractMusicSource() {

    private var catalog: List<MediaMetadataCompat> = emptyList()

    init {
        state = STATE_INITIALIZING
    }

    override fun iterator(): Iterator<MediaMetadataCompat> = catalog.iterator()

    override suspend fun load() {
        updateCatalog()?.let { newCatalog ->
            catalog = newCatalog
            state = STATE_INITIALIZED
        } ?: run {
            catalog = emptyList()
            state = STATE_ERROR
        }
    }

    private suspend fun updateCatalog(): List<MediaMetadataCompat> {
        return withContext(Dispatchers.IO) {
            val onlineTracks = async {
                repository.retrieveOnlineAudioTracks().map { song ->
                    MediaMetadataCompat.Builder()
                        .from(song, Origin.ONLINE)
                        .build()
                }.toList().onEach { it.description.extras?.putAll(it.bundle) }
            }

            val localTracks = async {
                repository.retrieveLocalAudioTracks().map { song ->
                    MediaMetadataCompat.Builder()
                        .from(song, Origin.LOCAL)
                        .build()
                }.toList().onEach { it.description.extras?.putAll(it.bundle) }
            }

            val recentTracks = async {
                repository.retrieveRecentTracks().map { song ->
                    MediaMetadataCompat.Builder()
                        .from(song, Origin.RECENT)
                        .build()
                }.toList().onEach { it.description.extras?.putAll(it.bundle) }
            }

            val favoriteTracks = async {
                repository.retrieveFavoriteTracks().map { song ->
                    MediaMetadataCompat.Builder()
                        .from(song, Origin.FAVORITES)
                        .build()
                }.toList().onEach { it.description.extras?.putAll(it.bundle) }
            }
            listOf(onlineTracks, localTracks, recentTracks, favoriteTracks).awaitAll().flatten()
        }
    }

    fun MediaMetadataCompat.Builder.from(track: AudioTrack, origin: Origin): MediaMetadataCompat.Builder {
        id = track.fileId.toString()
        title = track.title
        artist = track.artist
        genre = track.genre
        albumArtUri = track.image
        duration = TimeUnit.SECONDS.toMillis(track.duration.toLong())
        flag = when(origin) {
            Origin.PLAYLISTS -> MediaBrowser.MediaItem.FLAG_BROWSABLE
            else -> MediaBrowser.MediaItem.FLAG_PLAYABLE
        }
        mediaUri = track.source
        putString(KEY_ORIGIN, origin.name)
        putLong(KEY_FAVORITE, if (track.favourite) 1 else 0 ) // TODO: Redesign
        // To make things easier for *displaying* these, set the display properties as well.
        displayTitle = track.title
        displaySubtitle = track.artist
        displayDescription = track.album
        displayIconUri = track.image

        downloadStatus = when (origin) {
            Origin.ONLINE -> MediaDescriptionCompat.STATUS_NOT_DOWNLOADED
            Origin.LOCAL -> MediaDescriptionCompat.STATUS_DOWNLOADED
            else -> MediaDescriptionCompat.STATUS_DOWNLOADED
        }
        return this
    }
}
