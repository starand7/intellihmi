package com.automotive.bootcamp.mediaplayer.data.database.mapper

import com.automotive.bootcamp.mediaplayer.data.database.model.AudioMetadataDB
import com.automotive.bootcamp.mediaplayer.data.database.model.PlaylistDB
import com.automotive.bootcamp.mediaplayer.data.database.model.PlaylistTracksDB
import com.automotive.bootcamp.mediaplayer.data.database.model.RecentTrackDB
import com.automotive.bootcamp.mediaplayer.domain.model.AudioTrack

fun AudioTrack.toAudioMetadataDB(location: Int): AudioMetadataDB {
    return AudioMetadataDB(
        id = id,
        title = title,
        album = album,
        artist = artist,
        genre = genre,
        source = source ?: "", // TODO: remove empty string from source
        image = image,
        duration = duration,
        favourite = favourite,
        location = location
    )
}

fun AudioMetadataDB.toAudioTrack(): AudioTrack {
    return AudioTrack(
        id = id,
        title = title,
        album = album,
        artist = artist,
        genre = genre,
        source = source,
        image = image,
        duration = duration,
        favourite = favourite
    ).apply {
        fileId = trackId
    }
}

fun RecentTrackDB.toTrackId() = recentTrackId

fun Int.toRecentTrackDB() = RecentTrackDB(this)

fun Int.toCrossRef(id: Int) = PlaylistTracksDB(this, id)

fun String.toPlaylistName() = PlaylistDB(this)

fun PlaylistDB.toPlaylistName() = playlistName
