package com.automotive.bootcamp.mediaplayer.data.local.impl

import com.automotive.bootcamp.mediaplayer.data.local.LocalDataSource
import com.automotive.bootcamp.mediaplayer.data.local.mapper.toAudioTrack
import com.automotive.bootcamp.mediaplayer.data.local.metadata.LocalAudioTrackDataSource
import com.automotive.bootcamp.mediaplayer.domain.model.AudioTrack

class LocalDataSourceImpl(
    private val localAudioTrack: LocalAudioTrackDataSource
) : LocalDataSource {

    override fun getLocalMusic(): List<AudioTrack> {
        return localAudioTrack.getLocalAudioTracks().map {
            it.toAudioTrack()
        }
    }
}
