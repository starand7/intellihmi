package com.automotive.bootcamp.mediaplayer.presentation

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.core.net.toUri
import androidx.databinding.BindingAdapter
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import com.automotive.bootcamp.mediaplayer.R
import com.automotive.bootcamp.mediaplayer.exoplayer.toUri
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions

@BindingAdapter("imageUrl")
fun setImage(imageView: ImageView, imgUrl: String?) {
    imgUrl?.let {
        val glideUrl = imgUrl.toUri().buildUpon().scheme("https").build()

        val circularProgressDrawable = CircularProgressDrawable(imageView.context)
        circularProgressDrawable.apply {
            strokeWidth = 5f
            centerRadius = 30f
            start()
        }

        Glide.with(imageView.context)
            .load(glideUrl)
            .apply(
                RequestOptions()
                    .placeholder(circularProgressDrawable)
                    .error(R.drawable.artist)
            )
            .into(imageView)
    }
}

@BindingAdapter("titleText")
fun setText(textView: TextView, title: String?) {
    textView.text = title ?: R.string.unknown.toString()
}

@BindingAdapter("imageVisibility")
fun setImageVisibility(imageView: ImageView, visibility: Boolean) {
    imageView.visibility = if (visibility) View.VISIBLE else View.GONE
}