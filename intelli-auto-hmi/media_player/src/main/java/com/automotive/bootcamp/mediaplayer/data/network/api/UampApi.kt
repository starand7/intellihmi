package com.automotive.bootcamp.mediaplayer.data.network.api

import com.automotive.bootcamp.mediaplayer.data.network.model.OnlineAudioTrack
import kotlinx.coroutines.flow.Flow
import retrofit2.http.GET

interface UampApi {

    @GET("uamp/catalog.json")
    suspend fun fetchAudioTracks(): OnlineAudioTrack
}
