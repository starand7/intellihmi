package com.automotive.bootcamp.mediaplayer.presentation.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.PopupMenu
import androidx.core.view.isVisible
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.automotive.bootcamp.mediaplayer.R
import com.automotive.bootcamp.mediaplayer.databinding.TrackItemBinding
import com.automotive.bootcamp.mediaplayer.domain.model.AudioTrack
import com.automotive.bootcamp.mediaplayer.domain.model.MediaItemData
import com.automotive.bootcamp.mediaplayer.presentation.adapters.diffUtils.AudioTrackDiffCallback
import com.bumptech.glide.Glide

class AudioTrackListAdapter(
    private val onItemClick: (AudioTrack) -> Unit,
    private val onRemoveRecentClick: (AudioTrack) -> Unit,
    private val onToggleFavoriteStatus: (AudioTrack) -> Unit
) : ListAdapter<AudioTrack, AudioTrackListAdapter.MediaViewHolder>(AudioTrackDiffCallback) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MediaViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = TrackItemBinding.inflate(inflater, parent, false)
        return MediaViewHolder(binding, onItemClick, onRemoveRecentClick, onToggleFavoriteStatus)
    }

    override fun onBindViewHolder(
        holder: MediaViewHolder,
        position: Int
    ) {

        val mediaItem = getItem(position)
        holder.item = mediaItem
        with(holder.binding) {
            favouriteImage.isVisible = mediaItem.favourite
            trackTitle.text = mediaItem.title
            artistTitle.text = mediaItem.artist
            Glide.with(trackImage)
                .load(mediaItem.image)
                .placeholder(R.drawable.artist)
                .into(trackImage)
        }
        holder.binding.apply {
            track = mediaItem
            executePendingBindings()
        }
    }

    class MediaViewHolder(
        val binding: TrackItemBinding,
        onItemClick: (AudioTrack) -> Unit,
        onRemoveRecentClick: (AudioTrack) -> Unit,
        onToggleFavoriteStatus: (AudioTrack) -> Unit
    ) : RecyclerView.ViewHolder(binding.root) {

        var item: AudioTrack? = null

        init {
            with(binding) {
                trackImage.setOnClickListener {
                    item?.also(onItemClick)
                }
                favouriteImage.setOnClickListener {
                    item?.also(onToggleFavoriteStatus)
                }
                itemMenu.setOnClickListener {
                    PopupMenu(it.context, it).apply {
                        inflate(R.menu.options_menu)
                        setOnMenuItemClickListener { menuItem ->
                            when (menuItem.itemId) {
                                R.id.menu_play -> {
                                    item?.also(onItemClick)
                                    true
                                }
                                R.id.menu_remove_recent -> {
                                    item?.also(onRemoveRecentClick)
                                    true
                                }
                                R.id.menu_add_remove_favourites -> {
                                    item?.also(onToggleFavoriteStatus)
                                    true
                                }
                                else -> false
                            }
                        }
                        show()
                    }
                }
            }
        }
    }
}
