package com.automotive.bootcamp.mediaplayer.exoplayer.utils

import android.content.Context
import android.net.Uri
import com.google.android.exoplayer2.ExoPlayer
import com.google.android.exoplayer2.MediaItem
import com.google.android.exoplayer2.Player
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.flow.asStateFlow
import java.util.concurrent.TimeUnit

class TrackPlayer(context: Context) : Player.Listener {

    private val playerJob = Job()
    private val playerScope = CoroutineScope(Dispatchers.Main + playerJob)

    private val player = ExoPlayer.Builder(context).build()

    private var lastTrackUrl: String? = null

    // TODO: Suggest replace with SharedFlow -> To be discussed
    private val _audioDurationFlow = MutableStateFlow(0)
    val audioDurationFlow = _audioDurationFlow.asStateFlow()

    // TODO: Suggest replace with SharedFlow -> To be discussed
    private val _currentPlaytimePosition = MutableStateFlow(0)
    val currentPlaytimePosition = _currentPlaytimePosition.asStateFlow()

    private val _audioCompletionFlow = MutableSharedFlow<Unit>()
    val audioCompletionFlow = _audioCompletionFlow.asSharedFlow()

    init {
        player.addListener(this)
        updateDuration()
    }

    private fun updateDuration() {
        val audioDuration = if (player.contentDuration < 0) 0
        else TimeUnit.MILLISECONDS.toSeconds(player.contentDuration).toInt()
        val currentPlaytimePosition = TimeUnit.MILLISECONDS.toSeconds(player.currentPosition).toInt()
        playerScope.launch {
            _audioDurationFlow.emit(audioDuration)
            _currentPlaytimePosition.emit(currentPlaytimePosition)
            delay(1000L) // 1 second, so we can update the UI once per second
            updateDuration()
        }
    }

    override fun onPlaybackStateChanged(playbackState: Int) {
        super.onPlaybackStateChanged(playbackState)
        when (playbackState) {
            Player.STATE_ENDED -> {
                lastTrackUrl = null
                playerScope.launch {
                    _audioCompletionFlow.emit(Unit)
                }
            }
            Player.STATE_READY -> {
                player.play()
            }
        }
    }

    fun play(url: String?) {
        if (lastTrackUrl != url) {
            val item = MediaItem.fromUri(Uri.parse(url))
            player.setMediaItem(item)
            player.prepare()
        } else {
            if (!player.isPlaying) {
                player.seekTo(player.currentPosition)
                player.play()
            }
        }
        lastTrackUrl = url
    }

    fun pause() {
        player.pause()
    }

    fun resume() {
        player.play()
    }

    fun seekTo(time: Long) {
        player.seekTo(time * 1000) // Seconds
    }

    fun stop() {
        player.stop()
        player.release()
        playerScope.cancel()
    }
}
