package com.automotive.bootcamp.mediaplayer.presentation.adapters

import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.automotive.bootcamp.mediaplayer.R
import com.automotive.bootcamp.mediaplayer.exoplayer.library.Origin
import com.automotive.bootcamp.mediaplayer.presentation.media.MediaFragment
import com.automotive.bootcamp.mediaplayer.presentation.playlist.PlaylistFragment

class TabControlAdapter(fragment: Fragment) : FragmentStateAdapter(fragment) {

    override fun getItemCount() = tabs.count()

    override fun createFragment(position: Int): Fragment {
        return if (tabs[position].type == Origin.PLAYLISTS) PlaylistFragment()
        else MediaFragment.newInstance(tabs[position].type)
    }
}

val tabs = listOf(
    Tab(
        title = R.string.mp_tab_local,
        icon = R.drawable.ic_local,
        type = Origin.LOCAL
    ),
    Tab(
        title = R.string.mp_tab_online,
        icon = R.drawable.ic_online,
        type = Origin.ONLINE
    ),
    Tab(
        title = R.string.mp_tab_favourites,
        icon = R.drawable.ic_favourites,
        type = Origin.FAVORITES
    ),
    Tab(
        title = R.string.mp_tab_recent,
        icon = R.drawable.ic_recent,
        type = Origin.RECENT
    ),
    Tab(
        title = R.string.mp_tab_playlists,
        icon = R.drawable.ic_playlists,
        type = Origin.PLAYLISTS
    )
)

data class Tab(
    @StringRes val title: Int,
    @DrawableRes val icon: Int,
    val type: Origin
)
