package com.automotive.bootcamp.mediaplayer.data.network.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class OnlineAudioTrack(
    @Json(name = "music") val audioTrack: List<AudioTrack>
) {
    @JsonClass(generateAdapter = true)
    data class AudioTrack(
        @Json(name = "id") val id: String,
        @Json(name = "title") val title: String,
        @Json(name = "album") val album: String,
        @Json(name = "artist") val artist: String,
        @Json(name = "genre") val genre: String,
        @Json(name = "source") val source: String,
        @Json(name = "image") val image: String,
        @Json(name = "duration") val duration: Int
    )
}
