package com.automotive.bootcamp.mediaplayer.presentation.adapters.diffUtils

import androidx.recyclerview.widget.DiffUtil
import com.automotive.bootcamp.mediaplayer.domain.model.AudioTrack

object AudioTrackDiffCallback : DiffUtil.ItemCallback<AudioTrack>() {
    override fun areItemsTheSame(oldItem: AudioTrack, newItem: AudioTrack): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: AudioTrack, newItem: AudioTrack): Boolean {
        return oldItem == newItem
    }
}