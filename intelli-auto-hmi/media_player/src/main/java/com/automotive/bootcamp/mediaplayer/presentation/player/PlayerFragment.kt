package com.automotive.bootcamp.mediaplayer.presentation.player

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.Bundle
import android.os.IBinder
import android.util.Log
import android.view.View
import android.widget.SeekBar
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.flowWithLifecycle
import androidx.lifecycle.lifecycleScope
import com.automotive.bootcamp.mediaplayer.R
import com.automotive.bootcamp.mediaplayer.databinding.FragmentTrackBinding
import com.automotive.bootcamp.mediaplayer.domain.model.AudioTrack
import com.automotive.bootcamp.mediaplayer.exoplayer.library.Origin
import com.automotive.bootcamp.mediaplayer.exoplayer.utils.PlayerService
import com.automotive.bootcamp.mediaplayer.extensions.toTimeString
import com.automotive.bootcamp.mediaplayer.presentation.other.ARG_MEDIA_ID
import com.automotive.bootcamp.mediaplayer.presentation.other.ARG_MEDIA_TYPE
import com.automotive.bootcamp.mediaplayer.util.delegate.viewBindings
import com.bumptech.glide.Glide
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf


class PlayerFragment : Fragment(R.layout.fragment_track) {

    private val binding by viewBindings(FragmentTrackBinding::bind)
    private val viewModel by viewModel<PlayerViewModel> {
        val mediaId = arguments?.getInt(ARG_MEDIA_ID) ?: error("mediaId cannot be null here")
        val origin = arguments?.getString(ARG_MEDIA_TYPE) ?: error("origin cannot be null here")
        parametersOf(mediaId, origin)
    }

    private var playerService: PlayerService? = null
    private val connection = object : ServiceConnection {
        override fun onServiceConnected(name: ComponentName?, binder: IBinder?) {
            playerService = (binder as PlayerService.PlayerBinder).service
            playerService?.currentTrack?.observe(this@PlayerFragment.viewLifecycleOwner, this@PlayerFragment::fillPlayerView)
            playerService?.isPlaying?.observe(this@PlayerFragment.viewLifecycleOwner, this@PlayerFragment::fillPlaybackButton)
            playerService?.currentAudioDuration?.observe(this@PlayerFragment.viewLifecycleOwner) { duration ->
                if (duration != 0) {
                    binding.sbProgress.max = duration.toInt()
                    binding.tvDuration.text = duration.toTimeString()
                }
            }
            playerService?.currentPlaytime?.observe(this@PlayerFragment.viewLifecycleOwner) { progress ->
                binding.sbProgress.progress = progress.toInt()
                binding.tvProgress.text = progress.toTimeString()
            }
            viewModel.tracksStateFlow
                .flowWithLifecycle(this@PlayerFragment.viewLifecycleOwner.lifecycle)
                .onEach { playerService?.setPlaylistAndPlay(it) }
                .launchIn(this@PlayerFragment.viewLifecycleOwner.lifecycleScope)
        }

        override fun onServiceDisconnected(name: ComponentName?) {
            Log.d(TAG, "onServiceDisconnected: $name")
            playerService = null
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupViews()
        connectToService()
    }

    private fun setupViews() {
        setupBackButton()
        setupTogglePlaybackButton()
        setupProgressBar()
        setupNextButton()
        setupPreviousButton()
        setupShuffleButton()
    }

    private fun setupBackButton() {
        binding.btnBack.setOnClickListener {
            requireActivity().supportFragmentManager.popBackStack()
        }
    }

    private fun setupTogglePlaybackButton() {
        binding.ibTogglePlayback.setOnClickListener {
            playerService?.togglePlayback()
        }
    }

    private fun setupNextButton() {
        binding.ibNext.setOnClickListener {
            playerService?.next()
        }
    }

    private fun setupPreviousButton() {
        binding.ibPrevious.setOnClickListener {
            playerService?.previous()
        }
    }

    private fun setupShuffleButton() {
        binding.ibShuffle.setOnClickListener {
            playerService?.shuffle()
        }
    }

    private fun setupProgressBar() {
        binding.sbProgress.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(p0: SeekBar?, progress: Int, fromUser: Boolean) {
                if (fromUser) {
                    playerService?.seekTo(progress.toLong())
                }
            }

            override fun onStartTrackingTouch(p0: SeekBar?) { }

            override fun onStopTrackingTouch(p0: SeekBar?) { }
        })
    }

    private fun connectToService() {
        val serviceIntent = Intent(requireContext(), PlayerService::class.java)
        requireActivity().apply {
            bindService(serviceIntent, connection, Context.BIND_AUTO_CREATE)
            startService(serviceIntent)
        }
    }

    private fun fillPlaybackButton(isPlaying: Boolean) {
        val imageRes = if (isPlaying) R.drawable.ic_pause else R.drawable.ic_play
        binding.ibTogglePlayback.setImageResource(imageRes)
    }

    private fun fillPlayerView(track: AudioTrack) {
        with(binding) {
            Glide.with(ivAlbumArt)
                .load(track.image)
                .into(ivAlbumArt)
            tvTitle.text = track.title
            tvArtist.text = track.artist
        }
    }

    companion object {

        private val TAG = PlayerService::class.java.simpleName

        fun newInstance(mediaId: Int, origin: Origin) = PlayerFragment().apply {
            arguments = bundleOf(
                ARG_MEDIA_ID to mediaId,
                ARG_MEDIA_TYPE to origin.name
            )
        }
    }
}