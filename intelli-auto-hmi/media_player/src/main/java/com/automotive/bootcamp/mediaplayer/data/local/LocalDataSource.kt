package com.automotive.bootcamp.mediaplayer.data.local

import com.automotive.bootcamp.mediaplayer.domain.model.AudioTrack

interface LocalDataSource {

    fun getLocalMusic(): List<AudioTrack>
}
