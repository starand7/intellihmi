package com.automotive.bootcamp.mediaplayer.data.repository

import com.automotive.bootcamp.mediaplayer.data.database.AppDatabase
import com.automotive.bootcamp.mediaplayer.data.database.mapper.toAudioMetadataDB
import com.automotive.bootcamp.mediaplayer.data.database.mapper.toAudioTrack
import com.automotive.bootcamp.mediaplayer.data.database.mapper.toCrossRef
import com.automotive.bootcamp.mediaplayer.data.database.mapper.toPlaylistName
import com.automotive.bootcamp.mediaplayer.data.database.mapper.toRecentTrackDB
import com.automotive.bootcamp.mediaplayer.data.database.mapper.toTrackId
import com.automotive.bootcamp.mediaplayer.data.local.LocalDataSource
import com.automotive.bootcamp.mediaplayer.data.network.OnlineDataSource
import com.automotive.bootcamp.mediaplayer.domain.AudioLocation.LOCAL
import com.automotive.bootcamp.mediaplayer.domain.AudioLocation.ONLINE
import com.automotive.bootcamp.mediaplayer.domain.model.AudioTrack
import com.automotive.bootcamp.mediaplayer.domain.repository.AudioTrackRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

class AudioTrackRepositoryImpl(
    private val database: AppDatabase,
    private val onlineDataSource: OnlineDataSource,
    private val localDataSource: LocalDataSource
) : AudioTrackRepository {

    override val favouriteAudioTracks: Flow<List<AudioTrack>> =
        database.audioMetadataDao().getFavouriteTracks()
            .map { it.map { it.toAudioTrack() } }

    override val recentTracks: Flow<List<AudioTrack>> =
        database.recentTrackDao().getRecentTracks()
            .map {
                val ints = it.map { it.toTrackId() }
                database.audioMetadataDao().getTrackListByIds(ints)
                    .map { it.toAudioTrack() }.reversed()
            }

    override val playlists: Flow<List<String>> =
        database.playlistDao().getAudioPlaylist().map { it.map { it.toPlaylistName() } }

    override val onlineAudioTracks: Flow<List<AudioTrack>> =
        database.audioMetadataDao().getAudioMetadata(ONLINE.ordinal)
            .map { it.map { it.toAudioTrack() } }

    override val localAudioTracks: Flow<List<AudioTrack>> =
        database.audioMetadataDao().getAudioMetadata(LOCAL.ordinal)
            .map { it.map { it.toAudioTrack() } }

    override suspend fun retrieveOnlineAudioTrack() {
        val cachedTracks = database.audioMetadataDao().getAudioTracksByLocation(ONLINE.ordinal).map { it.toAudioTrack() }
        if (cachedTracks.isEmpty()) {
            onlineDataSource.fetchAudioTracks().also {
                database.audioMetadataDao().addAudioMetadata(it.map { it.toAudioMetadataDB(ONLINE.ordinal) })
            }
        }
    }

    override suspend fun retrieveLocalAudioTrack() {
        val tracks = localDataSource.getLocalMusic().map { it.toAudioMetadataDB(LOCAL.ordinal) }
        val currentTracks = database.audioMetadataDao().getAudioTracksByLocation(LOCAL.ordinal).map { it.id }
        val uniqueTracks = tracks.filterNot { currentTracks.contains(it.id) }
        database.audioMetadataDao().addAudioMetadata(uniqueTracks)
    }

    override suspend fun toggleTrackFavouriteStatus(track: AudioTrack) {
        database.audioMetadataDao().updateFavouriteStatus(track.fileId, !track.favourite)
    }

    override suspend fun appendRecentTrack(track: AudioTrack) {
        database.recentTrackDao().addRecentTrack(track.fileId.toRecentTrackDB())
    }

    override suspend fun removeRecentTrack(track: AudioTrack) {
        database.recentTrackDao().removeRecentTrack(track.fileId.toRecentTrackDB())
    }

    override suspend fun searchAudioTrack(word: String): List<AudioTrack> {
        return database.audioMetadataDao().searchAudioTrack("%$word%")
            .map { it.toAudioTrack() }
    }

    override suspend fun createNewPlaylist(playlistName: String) {
        return database.playlistDao().addNewPlaylist(playlistName.toPlaylistName())
    }

    override suspend fun retrieveTrackByPlaylist(playlistName: String): List<AudioTrack> {
        return database.playlistDao().getPlaylistTracks(playlistName).flatMap {
            it.tracks.map { it.toAudioTrack() }
        }
    }

    override suspend fun addNewTrackToPlaylist(playlistId: Int, trackId: Int) {
        database.playlistDao().addPlaylistItem(playlistId.toCrossRef(trackId))
    }

    override suspend fun deletePlaylist(playlistName: String) {
        database.playlistDao().deletePlaylistTracks(playlistName)
        database.playlistDao().removeAudioPlaylist(playlistName.toPlaylistName())
    }

    override suspend fun retrieveOnlineAudioTracks(): List<AudioTrack> {
        val metaDao = database.audioMetadataDao()
        val cachedTracks = metaDao
            .getAudioTracksByLocation(ONLINE.ordinal).map { it.toAudioTrack() }
        if (cachedTracks.isEmpty()) {
            return onlineDataSource.fetchAudioTracks().also {
                metaDao.addAudioMetadata(it.map {
                    it.toAudioMetadataDB(ONLINE.ordinal)
                })
            }
        }
        return cachedTracks
    }

    override suspend fun retrieveLocalAudioTracks(): List<AudioTrack> {
        val metaDao = database.audioMetadataDao()
        val tracks = localDataSource.getLocalMusic().map { it.toAudioMetadataDB(LOCAL.ordinal) }
        val currentTracks = metaDao.getAudioTracksByLocation(LOCAL.ordinal).map { it.id }
        val uniqueTracks = tracks.filterNot { currentTracks.contains(it.id) }
        metaDao.addAudioMetadata(uniqueTracks)
        return metaDao.getAudioTracksByLocation(LOCAL.ordinal).map { it.toAudioTrack() }
    }

    override suspend fun retrieveFavoriteTracks(): List<AudioTrack> {
        return database.audioMetadataDao().getFavoriteTracks().map { it.toAudioTrack() }
    }

    override suspend fun retrieveTrackById(mediaId: Int): AudioTrack? {
        val tracks = database.audioMetadataDao().getTrackListByIds(listOf(mediaId))
        return if (tracks.isNotEmpty()) tracks.first().toAudioTrack()
        else null
    }

    override suspend fun retrieveRecentTracks(): List<AudioTrack> {
        val tracks = database.recentTrackDao().getRecentTrack().map {
            it.toTrackId()
        }
        return database.audioMetadataDao().getTrackListByIds(tracks).map { it.toAudioTrack() }.reversed()
    }
}
