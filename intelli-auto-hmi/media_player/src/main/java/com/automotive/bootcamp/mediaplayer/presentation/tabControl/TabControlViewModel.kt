package com.automotive.bootcamp.mediaplayer.presentation.tabControl

import androidx.lifecycle.ViewModel
import com.automotive.bootcamp.mediaplayer.exoplayer.MusicServiceConnection
import com.automotive.bootcamp.mediaplayer.presentation.other.MEDIA_ROOT_ID

class TabControlViewModel(
    private val musicServiceConnection: MusicServiceConnection
) : ViewModel() {

    init {
        musicServiceConnection.subscribe(MEDIA_ROOT_ID)
    }

    fun playMediaId(mediaId: Int) {
        val transportControls = musicServiceConnection.transportControls
        transportControls.stop()
        transportControls.playFromMediaId(mediaId.toString(), null)
    }
}
