package com.automotive.bootcamp.mediaplayer.exoplayer.library

import android.content.Context
import android.support.v4.media.MediaBrowserCompat
import android.support.v4.media.MediaMetadataCompat
import com.automotive.bootcamp.mediaplayer.R
import com.automotive.bootcamp.mediaplayer.exoplayer.albumArtUri
import com.automotive.bootcamp.mediaplayer.exoplayer.flag
import com.automotive.bootcamp.mediaplayer.exoplayer.id
import com.automotive.bootcamp.mediaplayer.exoplayer.title
import com.automotive.bootcamp.mediaplayer.presentation.other.*

class BrowseTree(
    private val context: Context,
    musicSource: MusicSource
) {

    private val mediaIdToChildren = mutableMapOf<String, MutableList<MediaMetadataCompat>>()

    init {
        val rootList = mediaIdToChildren[MEDIA_BROWSABLE_ROOT] ?: mutableListOf()

        val localMetadata = MediaMetadataCompat.Builder().apply {
            id = MEDIA_LOCAL_ROOT
            title = context.getString(R.string.mp_tab_local)
            albumArtUri = RESOURCE_ROOT_URI +
                    context.resources.getResourceEntryName(R.drawable.ic_local)
            flag = MediaBrowserCompat.MediaItem.FLAG_BROWSABLE
        }.build()

        val onlineMetadata = MediaMetadataCompat.Builder().apply {
            id = MEDIA_ONLINE_ROOT
            title = context.getString(R.string.mp_tab_online)
            albumArtUri = RESOURCE_ROOT_URI +
                    context.resources.getResourceEntryName(R.drawable.ic_online)
            flag = MediaBrowserCompat.MediaItem.FLAG_BROWSABLE
        }.build()

        val recentMetadata = MediaMetadataCompat.Builder().apply {
            id = MEDIA_RECENT_ROOT
            title = context.getString(R.string.mp_tab_recent)
            albumArtUri = RESOURCE_ROOT_URI +
                    context.resources.getResourceEntryName(R.drawable.ic_recent)
            flag = MediaBrowserCompat.MediaItem.FLAG_BROWSABLE
        }.build()

        val favoritesMetadata = MediaMetadataCompat.Builder().apply {
            id = MEDIA_FAVORITES_ROOT
            title = context.getString(R.string.mp_tab_favourites)
            albumArtUri = RESOURCE_ROOT_URI +
                    context.resources.getResourceEntryName(R.drawable.ic_favourites)
            flag = MediaBrowserCompat.MediaItem.FLAG_BROWSABLE
        }.build()

        val playlistsMetadata = MediaMetadataCompat.Builder().apply {
            id = MEDIA_PLAYLISTS_ROOT
            title = context.getString(R.string.mp_tab_playlists)
            albumArtUri = RESOURCE_ROOT_URI +
                    context.resources.getResourceEntryName(R.drawable.ic_playlists)
            flag = MediaBrowserCompat.MediaItem.FLAG_BROWSABLE
        }.build()

        rootList += localMetadata
        rootList += onlineMetadata
        rootList += recentMetadata
        rootList += favoritesMetadata
        rootList += playlistsMetadata
        mediaIdToChildren[MEDIA_BROWSABLE_ROOT] = rootList

        musicSource.forEach { mediaItem ->
            when (mediaItem.bundle.getString(KEY_ORIGIN) ?: "") {
                Origin.ONLINE.name -> {
                    val onlineChildren = mediaIdToChildren[MEDIA_ONLINE_ROOT] ?: mutableListOf()
                    onlineChildren += mediaItem
                    mediaIdToChildren[MEDIA_ONLINE_ROOT] = onlineChildren
                }

                Origin.LOCAL.name -> {
                    val localChildren = mediaIdToChildren[MEDIA_LOCAL_ROOT] ?: mutableListOf()
                    localChildren += mediaItem
                    mediaIdToChildren[MEDIA_LOCAL_ROOT] = localChildren
                }

                Origin.RECENT.name -> {
                    mediaIdToChildren[MEDIA_RECENT_ROOT] = mutableListOf()
                }

                Origin.FAVORITES.name -> {
                    mediaIdToChildren[MEDIA_FAVORITES_ROOT] = mutableListOf()
                }

                Origin.PLAYLISTS.name -> {
                    mediaIdToChildren[MEDIA_PLAYLISTS_ROOT] = mutableListOf()
                }

                else -> {  }
            }
        }
    }

    operator fun get(mediaId: String) = mediaIdToChildren[mediaId]

    fun addMetadata(mediaId: String, children: MediaMetadataCompat) {
        val newChildren = mediaIdToChildren[mediaId] ?: mutableListOf()
        newChildren += children
        mediaIdToChildren[mediaId] = newChildren
    }

//    private fun buildAlbumRoot(mediaItem: MediaMetadataCompat): MutableList<MediaMetadataCompat> {
//        val albumMetadata = MediaMetadataCompat.Builder().apply {
//            putString(
//                MediaMetadataCompat.METADATA_KEY_MEDIA_ID, mediaItem.getString(
//                    MediaMetadataCompat.METADATA_KEY_ALBUM
//                ))
//            putString(
//                MediaMetadataCompat.METADATA_KEY_TITLE,
//                mediaItem.getString(MediaMetadataCompat.METADATA_KEY_ALBUM)
//            )
//            putLong(METADATA_KEY_FLAGS, MediaBrowserCompat.MediaItem.FLAG_BROWSABLE.toLong())
//        }.build()
//
//        val rootList = mediaIdToChildren[MEDIA_BROWSABLE_ROOT] ?: mutableListOf()
//        rootList += albumMetadata
//        mediaIdToChildren[MEDIA_BROWSABLE_ROOT] = rootList
//
//        return mutableListOf<MediaMetadataCompat>().also {
//            mediaIdToChildren[albumMetadata.getString(MediaMetadataCompat.METADATA_KEY_MEDIA_ID)] = it
//        }
//    }
}
