package com.automotive.bootcamp.mediaplayer.presentation.tabControl

import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.automotive.bootcamp.mediaplayer.R
import com.automotive.bootcamp.mediaplayer.databinding.FragmentPlayerBinding
import com.automotive.bootcamp.mediaplayer.presentation.adapters.TabControlAdapter
import com.automotive.bootcamp.mediaplayer.presentation.adapters.tabs
import com.automotive.bootcamp.mediaplayer.util.delegate.viewBindings
import com.google.android.material.tabs.TabLayoutMediator

class TabControlFragment : Fragment(R.layout.fragment_player) {

    private val binding by viewBindings(FragmentPlayerBinding::bind)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.vpTabs.adapter = TabControlAdapter(this)
        TabLayoutMediator(binding.tlTabs, binding.vpTabs) { tab, position ->
            tabs[position].also {
                with(tab) {
                    text = getString(it.title)
                    icon = ContextCompat.getDrawable(requireContext(), it.icon)
                }
            }
        }.attach()
    }
}
