package com.automotive.bootcamp.mediaplayer.data.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.automotive.bootcamp.mediaplayer.data.database.dao.AudioMetadataDao
import com.automotive.bootcamp.mediaplayer.data.database.dao.AudioPlaylistDao
import com.automotive.bootcamp.mediaplayer.data.database.dao.RecentTracksDao
import com.automotive.bootcamp.mediaplayer.data.database.model.AudioMetadataDB
import com.automotive.bootcamp.mediaplayer.data.database.model.PlaylistDB
import com.automotive.bootcamp.mediaplayer.data.database.model.PlaylistTracksDB
import com.automotive.bootcamp.mediaplayer.data.database.model.RecentTrackDB

@Database(
    entities = [
        AudioMetadataDB::class,
        RecentTrackDB::class,
        PlaylistDB::class,
        PlaylistTracksDB::class
    ],
    version = 1
)
abstract class AppDatabase : RoomDatabase() {

    abstract fun audioMetadataDao(): AudioMetadataDao
    abstract fun recentTrackDao(): RecentTracksDao
    abstract fun playlistDao(): AudioPlaylistDao

    companion object {
        private const val DB_NAME = "audio_metadata_db"
        private var instance: AppDatabase? = null

        fun getInstance(context: Context): AppDatabase {
            return instance ?: synchronized(this) {
                instance ?: createDatabase(context).also {
                    instance = it
                }
            }
        }

        private fun createDatabase(context: Context): AppDatabase {
            return Room.databaseBuilder(context, AppDatabase::class.java, DB_NAME)
                .build()
        }
    }
}
