package com.automotive.bootcamp.mediaplayer.exoplayer.utils

import android.app.Service
import android.content.Intent
import android.os.Binder
import android.os.IBinder
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.automotive.bootcamp.mediaplayer.domain.model.AudioTrack
import com.automotive.bootcamp.mediaplayer.domain.repository.AudioTrackRepository
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.collect
import org.koin.android.ext.android.inject

class PlayerService : Service() {

    private val serviceScope = CoroutineScope(Dispatchers.Main + Job())

    private val binder = PlayerBinder()

    override fun onBind(intent: Intent?): IBinder {
        return binder
    }

    inner class PlayerBinder : Binder() {
        val service: PlayerService
            get() = this@PlayerService
    }

    private val player by inject<TrackPlayer>()
    private val repository by inject<AudioTrackRepository>()

    private val _isPlaying = MutableLiveData<Boolean>()
    val isPlaying: LiveData<Boolean> = _isPlaying

    private val _currentTrack = MutableLiveData<AudioTrack>()
    val currentTrack: LiveData<AudioTrack> = _currentTrack

    private val _currentAudioDuration = MutableLiveData<Int>()
    val currentAudioDuration: LiveData<Int> = _currentAudioDuration

    // TODO: Suggest to unite this flow with `currentAudioDuration`
    private val _currentPlaytime = MutableLiveData<Int>()
    val currentPlaytime: LiveData<Int> = _currentPlaytime

    private var position = 0

    private var playlist = emptyList<AudioTrack>()

    init {
        serviceScope.launch {
            player.audioCompletionFlow.collect {
                next()
            }
        }
        serviceScope.launch {
            player.audioDurationFlow.collect { duration ->
                _currentAudioDuration.postValue(duration)
            }
        }
        serviceScope.launch {
            player.currentPlaytimePosition.collect { position ->
                _currentPlaytime.postValue(position)
            }
        }
    }

    // [clicked track, everything else ....]
    fun setPlaylistAndPlay(playlist: List<AudioTrack>) {
        this.playlist = playlist
        require(playlist.isNotEmpty()) {
            "Playlist shod not be empty!"
        }
        play(playlist.first())
    }

    fun play(track: AudioTrack) {
        _currentTrack.value = track
        // Need to add to recent
        player.play(track.source)
        _isPlaying.value = true
        serviceScope.launch {
            repository.appendRecentTrack(track)
        }
    }

    fun togglePlayback() {
        if (isPlaying.value == true) pause()
         else resume()

    }

    fun pause() {
        player.pause()
        _isPlaying.postValue(false)
    }

    fun resume() {
        player.resume()
        _isPlaying.postValue(true)
    }

    private fun playTrack(track: AudioTrack) {
        _currentTrack.postValue(track)
        play(track)
    }

    fun next() {
        if (++position == playlist.count()) {
            position = 0
        }
        val track = playlist[position]
        playTrack(track)
    }

    fun previous() {
        if (--position < 0) {
            position = playlist.count() - 1
        }
        val track = playlist[position]
        playTrack(track)
    }

    fun shuffle() {
        setPlaylistAndPlay(playlist.shuffled())
    }

    fun seekTo(time: Long) {
        player.seekTo(time)
    }

    override fun onDestroy() {
        serviceScope.cancel()
        super.onDestroy()
        player.stop()
    }
}
