package com.automotive.bootcamp.mediaplayer.data.network.mapper

import com.automotive.bootcamp.mediaplayer.data.network.model.OnlineAudioTrack
import com.automotive.bootcamp.mediaplayer.domain.model.AudioTrack
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

fun OnlineAudioTrack.toAudioTrack(): List<AudioTrack> {
    return audioTrack.map { item ->
        AudioTrack(
            id = item.id,
            album = item.album,
            title = item.title,
            artist = item.artist,
            genre = item.genre,
            source = item.source,
            image = item.image,
            duration = item.duration
        )
    }
}
