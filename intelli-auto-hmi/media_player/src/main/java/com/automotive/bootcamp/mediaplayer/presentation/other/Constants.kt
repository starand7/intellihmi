package com.automotive.bootcamp.mediaplayer.presentation.other

import com.automotive.bootcamp.mediaplayer.exoplayer.library.Origin

const val MEDIA_ROOT_ID = "MEDIA_ROOT_ID"
const val KEY_FAVORITE = "KEY_FAVORITE"
const val KEY_ORIGIN = "KEY_ORIGIN"

const val ARG_MEDIA_ID = "ARG_MEDIA_ID"
const val ARG_MEDIA_TYPE = "ARG_MEDIA_TYPE"


const val MEDIA_BROWSABLE_ROOT = "/"
val MEDIA_LOCAL_ROOT = Origin.LOCAL.name
val MEDIA_ONLINE_ROOT = Origin.ONLINE.name
val MEDIA_RECENT_ROOT = Origin.RECENT.name
val MEDIA_FAVORITES_ROOT = Origin.FAVORITES.name
val MEDIA_PLAYLISTS_ROOT = Origin.PLAYLISTS.name

const val RESOURCE_ROOT_URI = "android.resource://com.automotive.bootcamp.mediaplayer/drawable/"

const val METADATA_KEY_INTELLI_FLAGS = "com.automotive.bootcamp.mediaplayer.exoplayer.METADATA_KEY_INTELLI_FLAGS"
