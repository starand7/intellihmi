package com.automotive.bootcamp.mediaplayer.data.local.metadata

import android.annotation.SuppressLint
import android.content.Context
import android.database.Cursor
import android.graphics.Bitmap
import android.net.Uri
import android.util.Size
import com.automotive.bootcamp.mediaplayer.data.local.model.LocalAudioMetadata
import android.provider.MediaStore.Audio.Media as MediaStore

class LocalAudioTrackDataSource(
    private val context: Context
) {

    @SuppressLint("Recycle")
    fun getLocalAudioTracks(): List<LocalAudioMetadata> {

        val list = mutableListOf<LocalAudioMetadata>()
        val uri: Uri = MediaStore.INTERNAL_CONTENT_URI

        val cursor: Cursor? = context.contentResolver.query(
            uri, null, null, null, null
        )

        if (cursor != null && cursor.moveToFirst()) {
            val id = cursor.getColumnIndex(MediaStore._ID)
            val title = cursor.getColumnIndex(MediaStore.TITLE)
            val artist = cursor.getColumnIndex(MediaStore.ARTIST)
            val album = cursor.getColumnIndex(MediaStore.ALBUM)
            val duration = cursor.getColumnIndex(MediaStore.DURATION)
            val genre = cursor.getColumnIndex(MediaStore.GENRE)
            val image = cursor.getColumnIndexOrThrow(MediaStore.ALBUM_ID)
            val source = cursor.getColumnIndex((MediaStore.DATA))

            while (cursor.moveToNext()) {
                val audioId = cursor.getInt(id)
                val audioTitle = cursor.getString(title)
                val audioArtist = cursor.getString(artist)
                val audioAlbum = cursor.getString(album)
                val audioDuration = cursor.getString(duration)
                val audioGenre = cursor.getString(genre) ?: ""
                val audioImage = cursor.getLong(image)
                val audioSource = cursor.getString(source)

                list.add(
                    LocalAudioMetadata(
                        id = audioId,
                        title = audioTitle,
                        artist = audioArtist,
                        album = audioAlbum,
                        image = audioImage,
                        genre = audioGenre,
                        duration = audioDuration,
                        source = audioSource
                    )
                )
            }
        }

        return list
    }
}
