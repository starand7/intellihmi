package com.automotive.bootcamp.mediaplayer.exoplayer

import android.app.PendingIntent
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.ResultReceiver
import android.support.v4.media.MediaBrowserCompat
import android.support.v4.media.MediaDescriptionCompat
import android.support.v4.media.MediaMetadataCompat
import android.support.v4.media.RatingCompat
import android.support.v4.media.session.MediaSessionCompat
import android.support.v4.media.session.PlaybackStateCompat
import android.util.Log
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.media.MediaBrowserServiceCompat
import androidx.media.utils.MediaConstants
import com.automotive.bootcamp.mediaplayer.R
import com.automotive.bootcamp.mediaplayer.domain.repository.AudioTrackRepository
import com.automotive.bootcamp.mediaplayer.exoplayer.library.AutomotiveMusicSource
import com.automotive.bootcamp.mediaplayer.exoplayer.library.BrowseTree
import com.automotive.bootcamp.mediaplayer.exoplayer.library.MusicSource
import com.automotive.bootcamp.mediaplayer.presentation.other.MEDIA_RECENT_ROOT
import com.automotive.bootcamp.mediaplayer.presentation.other.MEDIA_ROOT_ID
import com.google.android.exoplayer2.C
import com.google.android.exoplayer2.ExoPlayer
import com.google.android.exoplayer2.PlaybackException
import com.google.android.exoplayer2.Player
import com.google.android.exoplayer2.audio.AudioAttributes
import com.google.android.exoplayer2.ext.mediasession.MediaSessionConnector
import com.google.android.exoplayer2.ext.mediasession.TimelineQueueNavigator
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.launch
import org.koin.android.ext.android.inject

class MediaPlaybackService : MediaBrowserServiceCompat() {

    private val repository by inject<AudioTrackRepository>()

    private var currentPlaylistItems: List<MediaMetadataCompat> = emptyList()
    private var curPlayingSong: MediaMetadataCompat? = null
    private var currentRoot: String? = null

    var isForegroundService = false

    private val serviceJob = SupervisorJob()
    private val serviceScope = CoroutineScope(Dispatchers.Main + serviceJob)

    protected lateinit var mediaSession: MediaSessionCompat
    protected lateinit var mediaSessionConnector: MediaSessionConnector

    private val mediaSource: MusicSource = AutomotiveMusicSource(repository)

    private val browseTree: BrowseTree by lazy {
        BrowseTree(applicationContext, mediaSource)
    }

    private val intelliAudioAttributes = AudioAttributes.Builder()
        .setContentType(C.CONTENT_TYPE_MUSIC)
        .setUsage(C.USAGE_MEDIA)
        .build()

    private val playerListener = PlayerEventListener()

    private val player: ExoPlayer by lazy {
        ExoPlayer.Builder(this).build().apply {
            setAudioAttributes(intelliAudioAttributes, true)
            setHandleAudioBecomingNoisy(true)
            addListener(playerListener)
        }
    }

    override fun onCreate() {
        super.onCreate()
        serviceScope.launch {
            mediaSource.load()
        }
        val sessionActivityPendingIntent = packageManager
            ?.getLaunchIntentForPackage(packageName)?.let { sessionIntent ->
                PendingIntent.getActivity(this, 0, sessionIntent, 0)
            }
        mediaSession = MediaSessionCompat(this, "MusicService").apply {
            setSessionActivity(sessionActivityPendingIntent)
            isActive = true
        }
        mediaSession.apply {
            setRatingType(RatingCompat.RATING_HEART)
            setRepeatMode(PlaybackStateCompat.REPEAT_MODE_ALL)
        }
        val playbackPreparer = IntelliPlaybackPreparer {
            curPlayingSong = it
        }
        mediaSessionConnector = MediaSessionConnector(mediaSession).apply {
            setPlayer(player)
            setPlaybackPreparer(playbackPreparer)
            setQueueNavigator(IntelliQueueNavigator(mediaSession))
            setEnabledPlaybackActions(
                PlaybackStateCompat.ACTION_PLAY_PAUSE
                        or PlaybackStateCompat.ACTION_PLAY
                        or PlaybackStateCompat.ACTION_PAUSE
                        or PlaybackStateCompat.ACTION_STOP
                        or PlaybackStateCompat.ACTION_SET_SHUFFLE_MODE
                        or PlaybackStateCompat.ACTION_SET_REPEAT_MODE
            )
        }
    }

    override fun onTaskRemoved(rootIntent: Intent?) {
        super.onTaskRemoved(rootIntent)
        player.stop()
        player.clearMediaItems()
    }

    override fun onDestroy() {
        mediaSession.run {
            isActive = false
            release()
        }
        serviceJob.cancel()
        player.removeListener(playerListener)
        player.release()
    }

    override fun onGetRoot(
        clientPackageName: String,
        clientUid: Int,
        rootHints: Bundle?
    ): BrowserRoot {
        val extras = bundleOf(
            MediaConstants.DESCRIPTION_EXTRAS_KEY_CONTENT_STYLE_PLAYABLE
                    to MediaConstants.DESCRIPTION_EXTRAS_VALUE_CONTENT_STYLE_GRID_ITEM
        )
        return BrowserRoot(MEDIA_ROOT_ID, extras)
    }

    override fun onLoadChildren(
        parentId: String,
        result: Result<List<MediaBrowserCompat.MediaItem>>
    ) {
        currentRoot = parentId
        val resultsSent = mediaSource.whenReady { successfullyInitialized ->
            if (successfullyInitialized && browseTree[parentId]?.isNotEmpty() == true) {
                val treeItem = browseTree[parentId]?.map {
//                    if (it.getLong(METADATA_KEY_FLAGS) == MediaBrowserCompat.MediaItem.FLAG_BROWSABLE.toLong()) {
//                        MediaBrowserCompat.MediaItem(
//                            MediaDescriptionCompat.Builder()
//                                .setTitle(it.getString(MediaMetadataCompat.METADATA_KEY_TITLE))
//                                .setMediaId(it.getString(MediaMetadataCompat.METADATA_KEY_MEDIA_ID))
//                                .setDescription(it.getString(MediaMetadataCompat.METADATA_KEY_TITLE))
//                                .build(),
//                            it.getLong(METADATA_KEY_FLAGS).toInt()
//                        )
//                    } else {
//                        MediaBrowserCompat.MediaItem(
//                            it.description,
//                            it.getLong(METADATA_KEY_FLAGS).toInt()
//                        )
//                    }
                    MediaBrowserCompat.MediaItem(it.description, it.flag)
                }?.toMutableList()
                result.sendResult(treeItem)
            } else {
                mediaSession.sendSessionEvent("error", null)
                result.sendResult(null)
            }
//            if (successfullyInitialized) {
//                val children = browseTree[parentId]?.map { item ->
//                    MediaBrowserCompat.MediaItem(item.description, item.flag)
//                }
//                result.sendResult(children)
//            } else {
//                mediaSession.sendSessionEvent("error", null)
//                result.sendResult(null)
//            }
        }
        if (!resultsSent) {
            result.detach()
        }
    }

    private fun preparePlaylist(
        metadataList: List<MediaMetadataCompat>,
        itemToPlay: MediaMetadataCompat?,
        playWhenReady: Boolean,
        playbackStartPositionMs: Long
    ) {
        val initialWindowIndex = if (itemToPlay == null) 0 else metadataList.indexOf(itemToPlay)
        currentPlaylistItems = metadataList

        player.stop()
        // Set playlist and prepare.
        player.setMediaItems(
            metadataList.map { it.toMediaItem() }, initialWindowIndex, playbackStartPositionMs)
        player.prepare()
        player.playWhenReady = playWhenReady
    }

    private fun saveRecent() {
        val item = currentPlaylistItems[player.currentMediaItemIndex]
        Log.d(TAG, "Recent song ${item.getString(MediaMetadataCompat.METADATA_KEY_TITLE)}")
        browseTree.addMetadata(MEDIA_RECENT_ROOT, item)
    }

    private inner class PlayerEventListener : Player.Listener {

        override fun onPlayerStateChanged(playWhenReady: Boolean, playbackState: Int) {
            when (playbackState) {
                Player.STATE_BUFFERING,
                Player.STATE_READY -> {
                    if (playbackState == Player.STATE_READY) {
                        saveRecent()
                        if (!playWhenReady) {
                            stopForeground(false)
                            isForegroundService = false
                        }
                    }
                }
                else -> { }
            }
        }

        override fun onPlayerError(error: PlaybackException) {
            var message = R.string.generic_error
            Log.e(TAG, "Player error: " + error.errorCodeName + " (" + error.errorCode + ")")
            if (error.errorCode == PlaybackException.ERROR_CODE_IO_BAD_HTTP_STATUS
                || error.errorCode == PlaybackException.ERROR_CODE_IO_FILE_NOT_FOUND) {
                message = R.string.error_media_not_found
            }
            Toast.makeText(
                applicationContext,
                message,
                Toast.LENGTH_LONG
            ).show()
        }
    }

    private inner class IntelliQueueNavigator(
        mediaSession: MediaSessionCompat
    ) : TimelineQueueNavigator(mediaSession) {
        override fun getMediaDescription(player: Player, windowIndex: Int): MediaDescriptionCompat {
//            if (windowIndex < currentPlaylistItems.size) {
//                return currentPlaylistItems[windowIndex].description
//            }
//            return MediaDescriptionCompat.Builder().build()
            return currentPlaylistItems[windowIndex].description
        }
    }

    private inner class IntelliPlaybackPreparer(
        private val playerPrepared: (MediaMetadataCompat?) -> Unit
    ) : MediaSessionConnector.PlaybackPreparer {

        override fun getSupportedPrepareActions(): Long =
            PlaybackStateCompat.ACTION_PREPARE_FROM_MEDIA_ID or
                    PlaybackStateCompat.ACTION_PLAY_FROM_MEDIA_ID

        override fun onPrepareFromMediaId(
            mediaId: String,
            playWhenReady: Boolean,
            extras: Bundle?
        ) {
            mediaSource.whenReady {
                val itemToPlay: MediaMetadataCompat? = mediaSource.find { item ->
                    item.id == mediaId
                }
                if (itemToPlay == null) {
                    Log.w(TAG, "Content not found: MediaID=$mediaId")
                    // TODO: Notify caller of the error.
                    Toast.makeText(
                        applicationContext,
                        "There is no audio for playback",
                        Toast.LENGTH_LONG
                    ).show()
                } else {
                    val playbackStartPositionMs = 0L
//                    val playbackStartPositionMs =
//                        extras?.getLong(MEDIA_DESCRIPTION_EXTRAS_START_PLAYBACK_POSITION_MS, C.TIME_UNSET)
//                            ?: C.TIME_UNSET
                    preparePlaylist(
                        buildPlaylist(itemToPlay),
                        itemToPlay,
                        playWhenReady,
                        playbackStartPositionMs
                    )
                    playerPrepared(itemToPlay)
                }
            }
        }

        override fun onPrepare(playWhenReady: Boolean) { }

        override fun onPrepareFromSearch(query: String, playWhenReady: Boolean, extras: Bundle?) { }

        override fun onPrepareFromUri(uri: Uri, playWhenReady: Boolean, extras: Bundle?) { }

        override fun onCommand(
            player: Player,
            command: String,
            extras: Bundle?,
            cb: ResultReceiver?
        ) = false

        private fun buildPlaylist(item: MediaMetadataCompat): List<MediaMetadataCompat> =
            mediaSource.filter { it.album == item.album }.sortedBy { it.trackNumber }
    }

    companion object {
        private val TAG = MediaPlaybackService::class.java.simpleName
    }
}
