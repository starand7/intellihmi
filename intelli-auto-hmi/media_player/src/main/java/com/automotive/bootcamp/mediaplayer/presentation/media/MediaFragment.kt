package com.automotive.bootcamp.mediaplayer.presentation.media

import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.flowWithLifecycle
import androidx.lifecycle.lifecycleScope
import com.automotive.bootcamp.mediaplayer.R
import com.automotive.bootcamp.mediaplayer.databinding.FragmentMediaBinding
import com.automotive.bootcamp.mediaplayer.exoplayer.library.Origin
import com.automotive.bootcamp.mediaplayer.presentation.adapters.AudioTrackListAdapter
import com.automotive.bootcamp.mediaplayer.presentation.other.ARG_MEDIA_TYPE
import com.automotive.bootcamp.mediaplayer.presentation.other.startTrackFragment
import com.automotive.bootcamp.mediaplayer.util.delegate.viewBindings
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf

class MediaFragment : Fragment(R.layout.fragment_media) {

    private val binding by viewBindings(FragmentMediaBinding::bind)
    private val viewModel by viewModel<MediaViewModel> {
        val type = arguments?.getString(ARG_MEDIA_TYPE) ?: error("ARG_MEDIA_TYPE cannot be null here!")
        parametersOf(type)
    }

    private val mediaAdapter by lazy {
        AudioTrackListAdapter(
            onItemClick = viewModel::mediaItemClicked,
            onRemoveRecentClick = viewModel::removeRecentTrack,
            onToggleFavoriteStatus = viewModel::toggleTrackFavoriteStatus
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.rvTracks.adapter = mediaAdapter
        viewModel.tracksFlow
            .flowWithLifecycle(viewLifecycleOwner.lifecycle)
            .onEach(mediaAdapter::submitList)
            .launchIn(viewLifecycleOwner.lifecycleScope)
        viewModel.selectedTrack
            .flowWithLifecycle(viewLifecycleOwner.lifecycle)
            .onEach { (id, origin) -> startTrackFragment(id, origin) }
            .launchIn(viewLifecycleOwner.lifecycleScope)
    }

    companion object {

        fun newInstance(type: Origin) = MediaFragment().apply {
            arguments = bundleOf(
                ARG_MEDIA_TYPE to type.name
            )
        }
    }
}
