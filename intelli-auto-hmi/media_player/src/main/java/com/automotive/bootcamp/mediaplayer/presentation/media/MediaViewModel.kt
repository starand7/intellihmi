package com.automotive.bootcamp.mediaplayer.presentation.media

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.automotive.bootcamp.mediaplayer.domain.model.AudioTrack
import com.automotive.bootcamp.mediaplayer.domain.repository.AudioTrackRepository
import com.automotive.bootcamp.mediaplayer.exoplayer.library.Origin
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.launch

class MediaViewModel(
    mediaId: String,
    private val audioTrackRepository: AudioTrackRepository,
) : ViewModel() {

    init {
        viewModelScope.launch {
            when(mediaId) {
                Origin.LOCAL.name -> audioTrackRepository.retrieveLocalAudioTrack()
                Origin.ONLINE.name -> audioTrackRepository.retrieveOnlineAudioTrack()
                else -> {}
            }
        }
    }

    val origin = when(mediaId) {
        Origin.LOCAL.name -> Origin.LOCAL
        Origin.ONLINE.name -> Origin.ONLINE
        Origin.RECENT.name -> Origin.RECENT
        Origin.FAVORITES.name -> Origin.FAVORITES
        else -> error("Unknown type here! $mediaId")
    }

    val tracksFlow = when(mediaId) {
        Origin.LOCAL.name -> audioTrackRepository.localAudioTracks
        Origin.ONLINE.name -> audioTrackRepository.onlineAudioTracks
        Origin.RECENT.name -> audioTrackRepository.recentTracks
        Origin.FAVORITES.name -> audioTrackRepository.favouriteAudioTracks
        else -> error("Unknown type here! $mediaId")
    }

    private val _selectedTrack = MutableSharedFlow<Pair<Int, Origin>>()
    val selectedTrack = _selectedTrack.asSharedFlow()

    fun mediaItemClicked(item: AudioTrack) {
        viewModelScope.launch {
            _selectedTrack.emit(Pair(item.fileId, origin))
        }
    }

    fun removeRecentTrack(track: AudioTrack) {
        viewModelScope.launch {
            audioTrackRepository.removeRecentTrack(track)
        }
    }

    fun toggleTrackFavoriteStatus(track: AudioTrack) {
        viewModelScope.launch {
            audioTrackRepository.toggleTrackFavouriteStatus(track)
        }
    }
}
