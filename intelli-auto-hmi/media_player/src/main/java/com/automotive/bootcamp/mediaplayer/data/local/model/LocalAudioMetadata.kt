package com.automotive.bootcamp.mediaplayer.data.local.model

data class LocalAudioMetadata(
    val id: Int,
    val title: String,
    val artist: String,
    val album: String,
    val image: Long,
    val genre: String,
    val duration: String,
    val source: String
)
