package com.automotive.bootcamp.launcher.presentation

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.commit
import com.automotive.bootcamp.hvaccontrol.presentation.HvacControlFragment
import com.automotive.bootcamp.launcher.R
import com.automotive.bootcamp.launcher.databinding.ActivityMainBinding
import com.automotive.bootcamp.mediaplayer.presentation.playback.PlaybackFragment
import com.automotive.bootcamp.mediaplayer.presentation.tabControl.TabControlFragment

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        supportActionBar?.hide()

        val binding = DataBindingUtil
            .setContentView<ActivityMainBinding>(this, R.layout.activity_main)

        navigateTo(TabControlFragment())

        binding.navigationRail.setOnItemSelectedListener { menuItem ->
            when (menuItem.itemId) {
                R.id.media_player_fragment -> {
                    navigateTo(TabControlFragment())
                    true
                }
                R.id.hvac_control_fragment -> {
                    navigateTo(HvacControlFragment())
                    true
                }
                else -> false
            }
        }

        supportFragmentManager.commit {
            replace(R.id.fl_bottom_menu, PlaybackFragment.newInstance())
        }
    }

    private fun navigateTo(fragment: Fragment) {
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.navigation_container, fragment)
            .commit()
    }
}
