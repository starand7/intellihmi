package com.automotive.bootcamp.launcher

import android.app.Application
import com.automotive.bootcamp.mediaplayer.di.mediaPlayerModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class App : Application() {

    override fun onCreate() {
        super.onCreate()
        initKoin()
    }

    private fun initKoin() {
        startKoin {
            androidContext(this@App)
            modules(mediaPlayerModule)
        }
    }
}
