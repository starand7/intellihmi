package com.automotive.bootcamp.launcher.presentation

import androidx.fragment.app.Fragment
import com.automotive.bootcamp.launcher.R

class BottomMenuFragment: Fragment(R.layout.bottom_menu_fragment)
